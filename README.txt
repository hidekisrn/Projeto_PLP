                                                            Paradigmas de Linguagens de Programação
                                                                        Turma C / 2018-1 
                                                                        Trabalho Semestral
    O Trabalho Semestral da disciplina Paradigmas de Linguagens de Programação consiste no estudo detalhado de uma linguagem de programação, observando os aspectos apresentados na disciplina, e sintetizando o estudo em uma midia que será apresentada para a turma. 
    A escolha da linguagem de programação é livre e de escolha dos dois integrantes da dupla de autores. Os trabalhos devem necessariamente ser feitos em duplas. Todos os trabalhos devem exemplificar a linguagem por um exemplo único - "determinar o menor e o maior elemento de um grupo de elementos" - sendo que a estrutura de dados e o algoritmo de implementação dependerá da linguagem a ser escolhida. 
    O Trabalho Semestral deverá ser entregue até o dia 18 de junho de 2018 (segunda feira), em um arquivo no formato pdf contendo: 
        1) integrantes da dupla de autores 
        2) titulo da linguagem de programação 
        3) link para o youtube que contém a midia de apresentação 
        4) listagem da implementação do exemplo 
    Obs-1: apenas um dos alunos integrantes da dupla de autores deverá postar o trabalho 
    Obs-2: O video deve ter no máximo 6 minutos e detalhar os aspectos da linguagem e exemplificar o uso da linguagem com o exemplo de verificação se um número inteiro dado é um número perfeito. 
    Verificar (conferir) se o audio está plenamente audível. 
    A apresentação dos Trabalhos Semestrais será feita no dia 22/junho/2018, sendo que a presença é obrigatória, pois trata-se de uma atividade com atribuição de nota. 
    Cada dupla tem um tempo de até 10 minutos para apresentar a midia que produziram e sanar duvidas dos alunos da turma. A aula iniciará as 14 horas e a sequência de apresentações seguirá uma tabela previamente estabelecida. 
    As notas do Trabalho Semestral será composta pela avaliação feita pelos pares (alunos, com peso de 70%) e pela avaliação feita pelo professor (peso de 30%). A avaliação dos trabalhos deverá considerar os seguintes itens: 
        · Originalidade do conteúdo: 20 pontos; · Relevância do conteúdo: 10 pontos; 
        · Abrangência e profundidade da pesquisa: 20 pontos 
        · Qualidade e concisão da mídia de apresentação: 50 pontos.